# API Reference Guide

[[_TOC_]]

## 1. [Functional Group Name]

_:bulb: Berikan deskripsi singkat mengenai functional umum bagi endpoint-endpoint yang termasuk kedalam group ini._

### 1.1 `[METHOD] [path/with/{slug-1}/if/any]`

_:bulb: Endpoint ini digunakan untuk..._

**Path Slugs:**

| **Key**    | **Description**      |
| :---       | :---                 |
| `slug-1`   | User ID              |
| `...`      | ... _dan seterusnya_ |

**Request Headers:**

| **Key**         | **Description**                                                                        |
| :---            | :---                                                                                   |
| `Authorization` | Token JWT untuk mengidentifikasi user yang melakukan request. Format: `Bearer <token>` |
| `...`           | ... _dan seterusnya_                                                                   |

**Request Body:**

| **Param**   | **Type** | **Options**        | **Description**                             |
| :---        | :---     | :---               | :---                                        |
| `category`  | `int`    |                    | ID katerogi                                 |
| `name`      | `string` |                    | Nama produk                                 |
| `color`     | `string` | `red, green, blue` | Warna produk. Pilih salah satu dari options |
| `size`      | `[int]`  |                    | Ukuran.                                     |
| `price.min` | `int`    |                    | Harga minimum                               |
| `price.max` | `int`    |                    | Harga minimum                               |
| `...`       | `...`    | `...`              | ... _dan seterusnya_                        |

**Response Code:**

| **Code** | **Description**               |
| ---:     | :---                          |
| `200`    | OK                            |
| `4xx`    | Ada yang salah dengan request |
| `5xx`    | Ada error di aplikasi         |
| `...`    | ... _dan seterusnya_          |
