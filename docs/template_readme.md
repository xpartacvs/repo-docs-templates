# Judul Aplikasi atau Service

_:bulb: Berikan deskripsi singkat mengenai aplikasi atau service yang dibuat_

[[_TOC_]]

## Kebutuhan Sistem

_:bulb: Tuliskan daftar apa-apa saja yang dibutuhkan untuk bekerja dan menjalankan aplikasi atau service ini di local. Contoh:_

- Go 1.15 keatas
- Credential ke server DB
- Credential ke stack lain (Redis, Kafka, dll)
- ...dan sebagainya

## Konfigurasi

| **ENV**                    | **YAML**                  | **env only** | **Req** | **Type** | **Options**                                             | **Default** | **Description**                                                                                               |
| :---                       | :---                      | :---:        | :---:   | :---     | :---                                                    | :---        | :---                                                                                                          |
| `ENV`                      |                           | √            | √       | string   | local, development, staging, production                 |             | Environment flag.                                                                                             |
| `LOG_LEVEL`                |                           | √            |         | string   | disabled, debug, info, warn, warning, err, error, fatal | disabled    | Log level.                                                                                                    |
| `CONFIG_DIRECTORY_PATH`    |                           | √            |         | string   |                                                         |             | A directory path of the rest configuartion files. When `ENV != local`, it becomes a mandatory.                |
| `CONFIG_ENCRYPTED`         |                           | √            |         | boolean  |                                                         | false       | When `true` the config files will be decrypted on preload.                                                    |
| `STORAGE_DIRECTORY_PATH`   |                           | √            | √       | string   |                                                         |             | A directory path to store imported/exported files.                                                            |
| `JWT_KEY`                  | `jwt.key`                 |              | √       | string   |                                                         |             | Backend JWT secret key.                                                                                       |
| `DB_HOST`                  | `db.host`                 |              | √       | string   |                                                         |             | Database server hostname/ip.                                                                                  |
| `DB_...`                   | `db....`                  |              |         | string   |                                                         | 5432        | Database server port.                                                                                         |
| `REDIS_HOST`               | `redis.host`              |              | √       | string   |                                                         |             | Redis server hostname/ip.                                                                                     |
| `REDIS_...`                | `redis....`               |              |         | string   |                                                         | 6379        | Redis server port.                                                                                            |
| `...`                      | `...`                     |              |         |          |                                                         |             | ... _dan seterusnya_                                                                                          |

## Cara Menjalankan di Localhost

_:bulb: Tuliskan langkah-langkah yang harus dilakukan untuk menjalankan aplikasi atau service ini di local. Contoh:_

- Pastikan stack-stack yang menjadi dependency (DB, Message Broker, dll) sudah berjalan sebelum menjalankan aplikasi ini
- Pastikan konfigurasi sudah benar
- Jalankan command ```go run main.go```
- ... dan seterusnya

## Branching Strategy

| Branch Name   | Description                     |
| :---          | :---                            |
| `master`      | _Branch ini digunakan untuk..._ |
| `development` | _Branch ini digunakan untuk..._ |
| `production`  | _Branch ini digunakan untuk..._ |
| `hotfix`      | _Branch ini digunakan untuk..._ |
| `feature/...` | _Branch ini digunakan untuk..._ |
| `...`         | _...dan seterusnya_             |

## Deployment

_:bulb: Tuliskan langkah-langkah yang harus dilakukan untuk melakukan deployment aplikasi atau service ini. Contoh:_

- Pastikan docker swarm sudah berjalan
- Deploy dengan perintah ```docker stack deploy -c docker-compose.yml <nama-stack>```

## Struktur Direktori (optional)

_:bulb: Jelaskan kegunaan masing-masing direktori yang dirasa penting untuk diketahui developer yang bekerja di service ini._

```code
-internal
  -app
    -config
    -handler
    -middleware
    -model
    -repository
    -service
-pkg
  -util
  -...
-test
  -...
-main.go
-...
```

- **internal:** Direktori ini berisi kode-kode yang tidak boleh diakses oleh service lain. Direktori ini dibagi menjadi beberapa sub-direktori:
  - **app:** Direktori ini berisi kode-kode yang berhubungan dengan aplikasi. Direktori ini dibagi menjadi beberapa sub-direktori:
    - **config:** Direktori ini berisi kode-kode yang berhubungan dengan konfigurasi aplikasi.
    - **handler:** Direktori ini berisi kode-kode yang berhubungan dengan handler.
    - **middleware:** Direktori ini berisi kode-kode yang berhubungan dengan middleware.
    - **model:** Direktori ini berisi kode-kode yang berhubungan dengan model.
    - **repository:** Direktori ini berisi kode-kode yang berhubungan dengan repository.
    - **service:** Direktori ini berisi kode-kode yang berhubungan dengan service.
- **pkg:** Direktori ini berisi kode-kode yang berhubungan dengan package yang dibuat oleh developer.
  - **util:** Direktori ini berisi kode-kode yang berhubungan dengan utility.
    - **...:** Direktori ini berisi kode-kode yang berhubungan dengan package lainnya.
- **test:** Direktori ini berisi kode-kode yang berhubungan dengan test.
  - **...:** Direktori ini berisi kode-kode yang berhubungan dengan test lainnya.
